const express = require('express');
const router = express.Router();
const Client = require('../models/clientSchema');

//error function
const handleError = (error) => {
    let errors = { email: "", phone: ""}

    //duplicate errors
    if (error.code == 11000 ) {
        if(error.message.includes("email_1 dup key")){
        errors.email = "Email already exists";
        return errors;
        }else if(error.message.includes("phone_1 dup key")){
            errors.phone = "Phone number already exists";
            return errors;
        }
    }

}





router.get('/', async (req, res) => {
  try {
    const client = await Client.find();
    res.json(client);
  } catch (error) {
    res.send("Error : " + error.message);
  }

});


router.get('/:id', async (req,res) => {
    try {
        const client = await Client.findById(req.params.id);
        res.json(client);
    } catch (error) {
        res.send("Error : " + error.message);
    }
});

router.post('/', async (req, res) => {
    
    const client = new Client({
        company_name : req.body.company_name,
        client_id : req.body.client_id,
        contact_person : req.body.contact_person,
        email : req.body.email,
        phone : req.body.phone,
        status : req.body.status
    });

    try {
        const clientCreate = await client.save();
        return res.json({ status: "success", message: "Record added successfully!!!!!!"});
    } catch (error) {
        const errors = handleError(error);
        res.json({errors});
    }

});

router.put('/:id', async (req, res) => {
    try {
        const client = await Client.findById(req.params.id);
        client.company_name = req.body.company_name;
        client.client_id = req.body.client_id;
        client.contact_person = req.body.contact_person;
        client.email = req.body.email;
        client.phone = req.body.phone;
        client.status = req.body.status;
        const clientUpdateFully = await client.save();
        res.json({status: "success", message: "Data updated Successfully",clientUpdateFully});
    } catch (error) {
        res.send("Error : " + error.message);
    }
})

router.patch('/:id', async (req,res) => {
    try {
        const client = await Client.findById(req.params.id);
        client.contact_person = req.body.contact_person;
        client.status = req.body.status;
        const clientUpdate = await client.save();
        res.send({status: "success", message: "Data updated Successfully",clientUpdate});
    } catch (error) {
        res.send("Error : " + error.message);
    }
});

router.delete('/:id', async (req,res) => {
    try {
        const client = await Client.findByIdAndRemove(req.params.id);
        const clientRemove = await client.remove();
        res.send({status: "Success", message: "Data deleted Successfully",clientRemove});
    } catch (error) {
        res.send("Error : " + error.message)
    }
});

module.exports = router;
