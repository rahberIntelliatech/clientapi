const express = require('express');
const mongoose = require('mongoose');
const url = "mongodb://127.0.0.1:27017/clients"


const app = express();

mongoose.connect(url, {useNewUrlParser:true})

const con = mongoose.connection;

app.use(express.json());

const clientRouters = require('./routes/routes')
app.use('/clients', clientRouters);


con.on('open', () => {
    console.log("DB Connected !!!");
});

app.listen(8080, () => {
    console.log("Server Started")
});
